# OS-Prober (Detecting other operating systems)
OS-Prober is a command-line utility that detects other operating systems on your device.
We use that in conjunction with the `grub-mkconfig` command in order to detect any operating systems that haven't been automatically picked up.

## Installation

Manjaro:

    sudo pacman -S os-prober

Ubuntu:

    sudo apt install os-prober

![../images/os-prober-install](../images/os-prober-install.png)

## Running
OS-Prober must be ran as run in order to work properly.

    sudo os-prober

![../images/os-prober](../images/os-prober.png)

As you can see, I have a Windows partition on `/dev/nvme0n1p1`. In order for grub to detect this, I need to mount it and then re-run the grub configuration tool. 

I will mount my Windows partition to a temporary directory in order for it to be detected.
**Note**: We ignore everything past the `@`  in the file path, we strictly only care about the drive location.

	sudo mount /dev/nvme0n1p1 /mnt

![../images/mounting-a-drive.png](../images/mounting-a-drive.png)

As you can see, you should now be able to `ls` the directory where the drive is mounting to if done successfully.

Now that you've mounted the relevant drive, you can run

	sudo grub-mkconfig 

This will then generate a file for you and sets it to GRUB's new configuration.

![../images/grub-mkconfig.png](../images/grub-mkconfig.png)


This should fix your GRUB config file, and now Windows should be detected on your boot drives menu in GRUB.


## Removal
Manjaro:

    sudo pacman -Rs os-prober

Ubuntu:

    sudo apt remove os-prober
