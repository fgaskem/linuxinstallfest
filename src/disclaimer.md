# README before anything else!

<span style="color:red; font-weight:bold">Warning! <br />
If you are installing Linux as a replacement OS or <em>dualboot</em>, please make sure
you have a <em>backup</em> of any data you do not want to lose. We are providing the
instructions as they are, and <em>we are not responsible for any data loss that
might occur</em>. By using them you agree <em>you are assuming your own risk</em>.
</span>

If you are installing it as a replacement OS or dualboot, you will need:
- a **USB**, of at least **4 GB**
- a drive imager. The Raspberry Pi foundation keeps an up-to-date list of [imagers for 
Windows](https://www.raspberrypi.org/documentation/installation/installing-images/windows.md),
however, we'd recommend [Etcher](https://www.balena.io/etcher/).
- image of the OS you want to install, either:
  - Ubuntu: [Ubuntu](https://ubuntu.com/download/desktop)
  **or**
  - Manjaro: [Manjaro](https://manjaro.org/downloads/official/gnome/)

If you are doing a VM install, you will only need the image of the OS you want to install, either:
  - Ubuntu: [Ubuntu](https://ubuntu.com/download/desktop)
  **or**
  - Manjaro: [Manjaro](https://manjaro.org/downloads/official/gnome/)
