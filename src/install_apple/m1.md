# Installing Linux on Apple Silicon (eg M1)

This guide will help you install Linux on newer Apple hardware that uses an Apple Silicon (ARM) CPU.

The Apple Silicon processors have very impressive performance for their energy consumption; no need for fans, all day battery life, great speed etc...but as they don't use the same CPU architecture as most desktop/laptop computers, a bit of extra work is required to get Linux working on them.

The main thing to keep in mind is that there's a difference between virtualization (running another OS designed for the same CPU architecture) and emulation (running an OS designed for a different CPU architecture).

---

## Table of Contents
* [Installation Methods](#methods)
    * [Bare Metal](#bare-metal)
    * [Dual Boot](#dual-boot)
    * [VM](#VM)
* [Setting up a VM with UTM](#UTM)
    * [Expanding Storage](#UTM-storage-expand)
* [Setting up a VM with Parallels](#parallels)

---

## Installation methods <a name="methods"></a>


### ~~Bare Metal~~ <a name="bare-metal"></a>

Running linux 'bare metal' on AS hardware is technically possible but not really usable yet.  Per [this thread](https://www.reddit.com/r/linux/comments/ojd4p9/has_anybody_tried_to_run_linux_on_mac_m1_recently/) on Reddit:

> [T]here are two Linux kernels available for the M1, the upstream kernel maintained by the Asahi Linux team, and the Corellium Kernel maintain by uh well Corellium.
>
> Corellium is a company that specialises in virtualizing iOS, but they have another project called 'projectsandcastle' which is a port of both the Linux kernel and Android to select iPhones. They took that knowledge and immediately ported that to the M1, the result is them being the first to offer a full Linux desktop running on bare metal M1, but that comes at a cost: None of their patches are upstreamable (their modifications to the Linux kernel are considered hacky and won't be merged to the official Linux kernel)
>
> The Asahi Linux kernel takes a different approach, with them taking as much time and funding as possible to create something that can be included officially with the addition of them also working on graphics drivers for the M1 (the Corellium kernel is all software rendering). However, as of now, they got as far as getting serial output (via UART) and simple framebuffer output (aka just enough to see the kernel output on the screen).

While technically these OSes 'run' they do not represent a really usable 'daily driver' setup and thus we don't recommend them at this time though if you're curious about what's currently possible, check out this [blog post](https://www.corellium.com/blog/linux-m1).

### ~~Dual Boot~~ <a name="dual-boot"></a>
Because there is no bare metal Linux OS that's ready yet, this option is not really viable either.

### Virtual Machine (VM)  <a name="VM"></a>
The remaining option, running Linux in a VM, is our recommendation.

Sadly, VirtualBox (our recommendation for running VMs on non-AS Apple hardware) [doesn't (and likely won't)](https://forums.virtualbox.org/viewtopic.php?f=8&t=100875) run on this new hardware.  That leaves two options:

* UTM (donationware, recommended)
* Parallels Desktop (non-free, though there's a free trial available)

## Setting up a Linux VM with UTM <a name="UTM"></a>
Except for the following small notes here (read them first), you can simply follow the guide [here](https://mac.getutm.app/gallery/ubuntu-20-04).  Note the troubleshooting section at the bottom!

1. You should first download the UTM app from one of the two links at the top of the page [here](https://mac.getutm.app/).  Don't try to install it via github as the guide suggests -- it's not necessary!  Note:

    > UTM is and always will be completely free and open source. The Mac App Store version is identical to the free version and there are no features left out of the free version. The only advantage of the Mac App Store version is that you can get automatic updates. Purchasing the App Store version directly funds the development of UTM and shows your support .

2. In step 3, you can leave "System" on the default (which should be something like:

    `QEMU 6.1 ARM Virtual Machine (alias of virt-6.1) (virt)`)

That's it!

### Expanding your UTM VM's storage <a name="UTM-storage-expand"></a>

If you want to expand your VM's storage after installation and don't want to re-install from scratch, you can use this process to expand your disk.

Ubuntu 20 uses the LVM system for managing disks, volumes and partitions; while this system is a bit complicated, it does make this sort of operation fairly straightforward.  For more information about LVM and resizing, see this [guide](https://www.rootusers.com/lvm-resize-how-to-increase-an-lvm-partition/).

*NOTE*: This process works but can be quite error prone; PLEASE PLEASE PLEASE make a backup as explained in step 4!

1. Setup / Information gathering
    * Start your VM, run a shell and run `lsblk /dev/vda`
        ```bash
        NAME                      MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
        vda                       252:0    0   20G  0 disk 
        ├─vda1                    252:1    0  512M  0 part /boot/efi
        ├─vda2                    252:2    0    1G  0 part /boot
        └─vda3                    252:3    0 18.5G  0 part 
          └─ubuntu--vg-ubuntu--lv 253:0    0 18.5G  0 lvm  /
        ```
    * There are three important parts of this output:
        * The 2nd line (vda) is the size of the 'virtual' disk.
			* It is 20GB, the size of the virtual disk from when the VM was initially set up.
        * The 5th line (vda3) is the size of the LVM VG or Volume Group
			* It is 18.5G, most of the 20GB disk.
        * The 6th line (ubuntu--vg-ubunut--lv) is the size of the LVM LG or Logical Volume.
			* It occupies the full 18.5GB of the VG.
    * We're going to need to resize all three one after another before we can expand our filesystem to take advantage of the space.
    * Now power down your VM; This is very important!

2. Install `qemu` on your mac
    * Under the hood, UTM is a nice GUI around the open source [qemu](https://www.qemu.org) emulator/virtualizer.
    * On your mac, install the [homebrew package manager](https://brew.sh).
    * Using homebrew, install the qemu tools suite with `brew install qemu`

3. Find your VM's data:
    * Right-click on your vm in the UTM sidebar and click `Show in Finder`
    * Quit UTM
    * In the finder window that pops up, find `<VM NAME>.utm`.  This 'file' is really a [bundle](https://en.wikipedia.org/wiki/Bundle_(macOS)).
    * Right-click the file and select "Show Package Contents" to go inside the bundle.

4. Make a backup
    * Within the images directory, you'll see a `qcow2` file that is your VM's virtual hard disk.
    * **IMPORTANT**: Make a duplicate (backup) of this file now in case the disk expansion fails.
    * If your VM stops booting at any point, simply quit UTM, and replace your qcow2 disk image with *a copy* of the backup you just made.

5. Resize Part 1: Expand the disk
    * On your mac, open terminal and run `qemu-img resize $PATH +$SIZE` where:
        * **$PATH** is the path to the qcow2 image
        * **$SIZE** is the amount to add eg *20G*
        * Don't forget the plus sign!
    * For example, this will add 10GB to the size of my disk:
        * `qemu-img resize disk-0.qcow2 +10G`
    * Note that this won't change the size of the qcow2 file from macOS' perspective.
    * You're now done on the Mac; all subsquent commands should be run in the VM.
    * Relaunch UTM, boot up your VM, open a shell and rerun `lsblk /dev/vda`:
        ```bash
        NAME                      MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
        vda                       252:0    0   30G  0 disk 
        ├─vda1                    252:1    0  512M  0 part /boot/efi
        ├─vda2                    252:2    0    1G  0 part /boot
        └─vda3                    252:3    0 18.5G  0 part 
          └─ubuntu--vg-ubuntu--lv 253:0    0 18.5G  0 lvm  /
        ```
    * This should confirm the larger size of the vda disk (20GB --> 30GB); however our vda3 VG and our LV have not changed in size.

6. Resize Part 2: Expand the Volume Group
    * Install `gparted` with `sudo apt-get install gparted`.
    * Run gparted with `sudo gparted`.
    * You should get a popup message "Not all of the space available to /dev/vda..."
		* Click "fix".
    * Note the gray box of unallocated space to the far right of the disk visualization.
    * Next, select the /dev/vda3 partition in the list, right click it and select "Resize/Move"
    * In the popup, slide the right edge of the graphical bar at the top all the way to the right to expand the partition to use that space and hit "Resize".
        * There may be a small amount of unallocated space left after this; don't worry!
    * Hit the green checkmark icon to apply the changes; in the popup, click apply to confirm.
    * After this completes, rerun `lsblk /dev/vda`:
        ```
        NAME                      MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
        vda                       252:0    0   30G  0 disk 
        ├─vda1                    252:1    0  512M  0 part /boot/efi
        ├─vda2                    252:2    0    1G  0 part /boot
        └─vda3                    252:3    0 28.5G  0 part 
          └─ubuntu--vg-ubuntu--lv 253:0    0 18.5G  0 lvm  /
        ```
    * Now we can see that the vda3 partition has been expanded however our Logical Volume is still 18.5GB.

7. Resize Part 3: Expand the Logical Volume
    * We now can grow our logical volume (LV) to fill the full volume group
    * This should be as simple as `sudo lvextend -L+10G /dev/ubuntu-vg/ubuntu-lv`
        * Note that the size (eg: the 10G on the line above) *must* match the size you expanded your disk image by in step 5.
    * Confirm the resize:
        ```bash
        NAME                      MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
        vda                       252:0    0   30G  0 disk 
        ├─vda1                    252:1    0  512M  0 part /boot/efi
        ├─vda2                    252:2    0    1G  0 part /boot
        └─vda3                    252:3    0 28.5G  0 part 
          └─ubuntu--vg-ubuntu--lv 253:0    0 28.5G  0 lvm  /
        ```
	* Looking good!

8. Resize Part 4: Expand the filesystem
	* The final step -- now that we've created extra space in our logical volume, we need to expand our filesystem to use that space.
	* Run `df -h` to get information on how much space is available for each filesystem.
	* For the default ext4 fs, the `resize2fs` command allows you to expand the allocation:
		* `resize2fs /dev/ubuntu-vg/ubuntu-lv`
	* re-run `df -h` to confirm the new space; hopefully the filesystem has grown!

9. Reboot, for good measure!
	* Does the VM boot?
	* Confirm everything (`lsblk /dev/vda`, `df -f`) look good.

### Setting up a Linux VM with Parallels <a name="parallels"></a>

Simply follow the guide [here](https://kb.parallels.com/128445).  Parallels costs 70 GBP a year after the free trial ends.

Supported Linux OSes:
* Ubuntu Linux 21.04, 20.10, 20.04
* Fedora Workstation 34, 33-1.2
* Debian GNU/Linux 10.7
* Kali Linux 2021.2, 2021.1

Make sure to choose 64-bit ARM installation media; here are some links:
* [Kali 2021.3 installer iso](https://cdimage.kali.org/kali-2021.3/kali-linux-2021.3-installer-arm64.iso) (3.4 GB)
* [Kali 2021.3 installer torrent](https://kali.download/base-images/kali-2021.3/kali-linux-2021.3-installer-arm64.iso.torrent)
* [Ubuntu 20.04.3 LTS](https://cdimage.ubuntu.com/focal/daily-live/current/focal-desktop-arm64.iso) (2.1 GB)
* [Debian 10.7 iso](https://cdimage.debian.org/cdimage/release/current/arm64/iso-cd/debian-11.0.0-arm64-netinst.iso) (320 MB)
* [Debian 10.7 torrent](https://cdimage.debian.org/cdimage/release/current/arm64/bt-cd/debian-11.0.0-arm64-netinst.iso.torrent) 



<!-- ![../../images/ubuntu/vm-create-os.png](../../images/ubuntu/vm-create-os.png) -->

---

Enjoy your new VM!

If you like, you can head over to the [Post Installation](../../post-install.md) guide to find some additional tips for software/configuration but be advised that most of this page is not relevant to your setup!
